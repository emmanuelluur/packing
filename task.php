<?php
/**
 * Genera folio por un periodo
 * de tiempo programado en
 * CRON
 */
require_once "./models/folio.php";
require_once "./core/time_zone.php";
class CRON extends Folio {
  private $model;
  public function __construct(){
    $this->model = new Folio;
  }

  public function save(){
    $folio = new Folio;
    $folio->hora = DATE('H:i:s');
    $folio->fecha = DATE('Y-m-d');
    $folio->folio = DATE('H');
    $folio->create();
    echo "Folio {$folio->folio} Generado Correctamente";
  }

  public function close_active(){
    $folio = new Folio;
    $folio->estatus = 0;
    $folio->close();
  }
}


(new DefineTimeZone('US/Central')); /*Define Zona horaria*/
(new CRON)->close_active(); /* Cierra folio activo */
(new CRON)->save(); /* Genera nuevo folio */
