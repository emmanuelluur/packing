<?php
require_once "./core/crud.php";

class Folio extends CRUD
{
    private $pdo;
    private $table = 'tbl_folio_planta_1';
    public $id;
    public $hora;
    public $fecha;
    public $folio;
    public $estatus;

    public function __construct()
    {
        parent::__construct($this->table);
        $this->pdo = parent::_conn();
    }

    public function create()
    {
        try {
            $stm = $this->pdo->prepare("INSERT INTO {$this->table} (hora, fecha, folio) VALUES (?,?,?)");
            $stm->execute([$this->hora, $this->fecha, $this->folio]);
        } catch (\PDOException $e) {
            return $e->getMessage();
        }
    }

    public function update()
    {return 1;}

    public function close()
    {
        /* cierra todos loas activos */
        try {
            $stm = $this->pdo->prepare("UPDATE {$this->table} SET estatus=? WHERE estatus = 1");
            $stm->execute([$this->estatus]);
        } catch (\PDOException $e) {
            return $e->getMessage();
        }
    }
}
