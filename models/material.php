<?php
require_once "./core/crud.php";

class Material extends CRUD
{
    private $pdo;
    private $table = 'tbl_material';
    public $id;
    public $material;
    public $descripcion;
    public $mrp;
    public $vendor;
    public $programa;
    public $locacion_planta_1;
    public $locacion_planta_2;
    public $standard_pack;
    public $estatus;

    public function __construct()
    {
        parent::__construct($this->table);
        $this->pdo = parent::_conn();
    }

    public function create()
    {
        try {
            $stm = $this->pdo->prepare("INSERT INTO {$this->table} (material, descripcion, mrp, vendor, programa, locacion_planta_1, locacion_planta_2) VALUES (?,?,?,?,?,?,?)");
            $stm->execute([
                $this->material,
                $this->descripcion,
                $this->mrp,
                $this->vendor,
                $this->programa,
                $this->locacion_planta_1,
                $this->locacion_planta_2]);
        } catch (\PDOException $e) {
            return $e->getMessage();
        }
    }

    public function update()
    {
        try {
            $stm = $this->pdo->prepare("UPDATE {$this->table} SET material=?, descripcion=?, mrp=?, vendor=?, programa=?, locacion_planta_1=?, locacion_planta_2=? WHERE id=?");
            $stm->execute([
                $this->material,
                $this->descripcion,
                $this->mrp,
                $this->vendor,
                $this->programa,
                $this->locacion_planta_1,
                $this->locacion_planta_2,
                $this->id]);
        } catch (\PDOException $e) {
            return $e->getMessage();
        }

    }
}
