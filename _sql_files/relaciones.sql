USE adient_ramos_pull;
/* planta 1 */
ALTER TABLE tbl_material_folio_planta_1 ADD FOREIGN KEY (packingId) REFERENCES tbl_folio_planta_1(id) ON DELETE CASCADE;
ALTER TABLE tbl_material_folio_planta_1 ADD FOREIGN KEY (materialId) REFERENCES tbl_material(id) ON DELETE CASCADE;

/* planta 2 */
/*
ALTER TABLE tbl_material_folio_planta_2 ADD FOREIGN KEY (packingId) REFERENCES tbl_folio_planta_2(id) ON DELETE CASCADE;
ALTER TABLE tbl_material_folio_planta_2 ADD FOREIGN KEY (materialId) REFERENCES tbl_material(id) ON DELETE CASCADE;
*/
