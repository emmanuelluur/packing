USE adient_ramos_pull;
CREATE TABLE IF NOT EXISTS tbl_folio_planta_2
(
  id INT NOT NULL AUTO_INCREMENT,
  hora TIME,
  fecha DATE,
  folio INT,
  fecha_pta1 DATE,
  folio_pta1 INT,
  entregar VARCHAR(30),
  via VARCHAR(30),
  sello VARCHAR(30),
  estatus VARCHAR(1) DEFAULT 1,
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
  updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS tbl_material_folio_planta_2
(
  id INT NOT NULL AUTO_INCREMENT,
  packingId INT,
  materialId INT,
  cantidad INT DEFAULT 0,
  serial VARCHAR(60),
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
