USE adient_ramos_pull;

CREATE TABLE IF NOT EXISTS tbl_material
(
  id INT NOT NULL AUTO_INCREMENT,
  material VARCHAR(60),
  descripcion VARCHAR(90),
  mrp VARCHAR(60),
  vendor VARCHAR(60),
  programa VARCHAR(60),
  locacion_planta_1 VARCHAR(60),
  locacion_planta_2 VARCHAR(60),
  standard_pack INT DEFAULT 0,
  estatus VARCHAR(1) DEFAULT 1,
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
  updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS tbl_folio_planta_1
(
  id INT NOT NULL AUTO_INCREMENT,
  hora TIME,
  fecha DATE,
  folio INT,
  estatus VARCHAR(1) DEFAULT 1,
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
  updated_at DATETIME ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS tbl_material_folio_planta_1
(
  id INT NOT NULL AUTO_INCREMENT,
  packingId INT ,
  materialId INT,
  cantidad INT DEFAULT 0,
  comentario TEXT,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
