<?php
require_once "./core/time_zone.php";
(new DefineTimeZone('US/Central')); /*Define Zona horaria*/


if (!isset($_REQUEST['controller'])){
  require_once "Controller/Material_Controller.php";
  $controller = new Material_Controller;
  $controller->index();
} else {
  $action = $_REQUEST['action'];
  $controller =$_REQUEST['controller'];
  $file = "Controller/".$controller."_Controller.php";

  if (!file_exists($file)){
    _404();
  } else {
    require_once $file;
    $controller = ucwords($controller).'_Controller';
    $controller = new $controller;
    call_user_func([$controller, $action]); // ejecuta la funcion
  }

}


function _404() {
  echo "No se encontro nada :(";
}
