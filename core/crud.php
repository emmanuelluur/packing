<?php
require_once './Core/connection.php';

abstract class CRUD extends Connection {
  private $table;
  private $pdo;
  public function __construct($table){
    $this->table = $table;
    $this->pdo = parent::_conn();
  }

  public function get_all(){
    # Todos los registros de una tabla
    try {
      $stm = $this->pdo->prepare("SELECT * FROM {$this->table}");
      $stm->execute();
      return $stm->fetchAll(PDO::FETCH_OBJ);
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function get_by_id($id){
    try {
      $stm = $this->pdo->prepare("SELECT * FROM {$this->table} WHERE id=?");
      $stm->execute([$id]);
      return $stm->fetch(PDO::FETCH_OBJ);
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  public function delete($id){
    # Un elemento de una tabla
    try {
      $stm = $this->pdo->prepare("DELETE FROM {$this->table} WHERE id=?");
      $stm->execute([$id]);
    } catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  abstract function create(); # se requiere
  abstract function update(); # se requiere
}
