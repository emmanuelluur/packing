<?php

class Connection {
  private $_driver = 'mysql';
  private $_host = 'localhost';
  private $_user = 'root';
  private $_password = '';
  private $_dbname = 'adient_ramos_pull';
  private $_charset = 'utf8';

  protected function _conn(){
    try {
      $pdo = new PDO("{$this->_driver}:host={$this->_host};dbname={$this->_dbname};charset={$this->_charset};",$this->_user,$this->_password);
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      return $pdo;
    }catch(PDOException $e){
      echo $e->getMessage();
    }
  }
}
