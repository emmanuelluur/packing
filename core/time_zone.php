<?php
# Zona Horaria


class DefineTimeZone {
  /**
  * Clase para establecer zona horaria en php sin modificar el host
  * donde este el script php
  */
  function __construct($tmz){
    # al instanciar se requiere de la zona horaria ejm. US/Central
    $this->tmz = $tmz;
    return $this->set_new_time_zone();
  }
  function set_new_time_zone(){
    # Establece zona horaria
      return date_default_timezone_set($this->tmz);
  }
}
