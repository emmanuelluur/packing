<?php

require_once "./models/material.php";

class Material_Controller extends Material{
  private $model;
  private $hour;
  private $date;
  public function __construct(){
    $this->model = new Material;
  }
  public function index(){
    $data = $this->model;
    require_once './views/material_form.php';
  }
  public function show_material_id(){
    if (isset($_REQUEST['id'])){
      $material = $this->model;
      $result = $material->get_by_id($_REQUEST['id']);
    }
    require_once './views/material_form.php';
  }
  public function save(){
    $material = $this->model;
    $material->id = $_REQUEST['id'];
    $material->material = $_REQUEST['material'];
    $material->descripcion = $_REQUEST['descripcion'];
    $material->vendor = $_REQUEST['vendor'];
    $material->mrp = $_REQUEST['mrp'];
    $material->programa = $_REQUEST['programa'];
    $material->locacion_planta_1 = $_REQUEST['locacion_planta_1'];
    $material->locacion_planta_2 = $_REQUEST['locacion_planta_2'];

    if ($material->id > 0) {
      $material->update();
    }  else {
      $material->create();
    }
    
    header('Location: index.php?success');
  }
}
