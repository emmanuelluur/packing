<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Adient</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="./static/css/bootstrap.min.css">
  </head>
  <body>
    <section class="material_form">
      <div class="container pt-5 mt-3">
        <h1>Material</h1>
        <div class="row">
          <div class="col-6">
          <form action="index.php?controller=material&action=save" method="POST">
            <div class="form-group">
              Material:
              <input type="text" class="form-control" name='material' value = "<?php  echo (isset($_REQUEST['id'])) ? $result->material:""; ?>" autofocus>
            </div>
            <div class="form-group">
              Descripción:
              <input type="text" class="form-control" name='descripcion' value = "<?php  echo (isset($_REQUEST['id'])) ? $result->descripcion:""; ?>">
            </div>
            <div class="form-group">
              MRP:
              <input type="text" class="form-control" name='mrp' value = "<?php  echo (isset($_REQUEST['id'])) ? $result->mrp:""; ?>">
            </div>
            <div class="form-group">
              Vendor:
              <input type="text" class="form-control" name='vendor' value = "<?php  echo (isset($_REQUEST['id'])) ? $result->vendor:""; ?>">
            </div>
            <div class="form-group">
              Programa:
              <input type="text" class="form-control" name='programa' value = "<?php  echo (isset($_REQUEST['id'])) ? $result->programa:""; ?>">
            </div>
            <div class="form-group">
              Locación en planta 1:
              <input type="text" class="form-control" name='locacion_planta_1' value = "<?php  echo (isset($_REQUEST['id'])) ? $result->locacion_planta_1:""; ?>">
            </div>
            <div class="form-group">
              Locación en planta 2:
              <input type="text" class="form-control" name='locacion_planta_2' value = "<?php  echo (isset($_REQUEST['id'])) ? $result->locacion_planta_2:""; ?>">
            </div>
            <input type="hidden" name="id" value = "<?php  echo (isset($_REQUEST['id'])) ? $_REQUEST['id']:"0"; ?>">
            <?php if (isset($_REQUEST['success'])): ?>
            <div class="alert alert-success"> Elemento Guardado </div>
            <?php endif;?>
            <button type="submit" class="btn btn-info"> Guardar </button>
          </form>
          </div>
        </div>
      </div>
    </section>
    <script src="./static/js/jquery-3.4.1.min.js" charset="utf-8"></script>
    <script src="./static/js/popper.min.js" charset="utf-8"></script>
    <script src="./static/js/bootstrap.min.js" charset="utf-8"></script>
  </body>
</html>
